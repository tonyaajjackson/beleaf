import argparse
import os
from glob import glob
import re


def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        usage="%(prog)s [OPTION] [FILE]...",
        description="Pull a leaf-to-branch document from an Obsidian vault"
    )

    parser.add_argument(
        "folder_path",
        type=str,
        help="Path to the Obsidian vault top level folder"
    )
      
    parser.add_argument(
        "leaf_path",
        type=str,
        help="Path to the leaf file, which will be opened and then traced backwards from to the root"
    )
    

    return parser


parser = init_argparse()
args = parser.parse_args()

folder_path = args.folder_path if args.folder_path[0] != "." else os.getcwd() + args.folder_path[1:]
leaf_path = args.leaf_path if args.leaf_path[0] != "." else os.getcwd() + args.leaf_path[1:]

files = glob(folder_path + '/**/*.md', recursive=True)

current_file = leaf_path

trace = [leaf_path]
previously_visited = {leaf_path:"Starting leaf"}    # As "file_path", "from_file_path"

link_regex = re.compile(r"#backlink:\[\[([^\|]*)(?:\|.*)?\]\]")

while True:
    with open(current_file) as f:
        # print("Opening: " + current_file)
        head = f.readline()

        backlink = link_regex.match(head)

        if backlink:
            branch_file = folder_path + backlink[1] + ".md"

            if branch_file in previously_visited.keys():
                raise Exception("Cycle found! Just reached " +
                                branch_file +
                                " from " + 
                                current_file +
                                ", but had previously visited from" +
                                previously_visited[branch_file]
                                )
            
            
            # print("Found backlink to: " + branch_file)

            previously_visited[branch_file] = current_file

            trace.append(branch_file)
            
            current_file = branch_file
            
        else:
            # print("No backlink found, assuming root")
            break
            

while trace:
    with open(trace[-1]) as f:
        head = f.readline()
        backlink = link_regex.match(head)

        if backlink:
            print(f.read())
        else:
            f.seek(0)
            print(f.read())
        
        trace.pop()

print("end")