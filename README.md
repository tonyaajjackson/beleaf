# beleaf

A quick proof-of-concept for creating a document from a directed acyclic graph of text nodes in an Obsidian vault. Credit to [@meditationstuff](https://twitter.com/meditationstuff/status/1740121460838899954) for the idea.

# Prerequisites
This project is using [Pipenv](https://pipenv.pypa.io/en/latest/) for package management - see the [Pipenv Installation Instructions](https://pipenv.pypa.io/en/latest/installation.html) to get it set up.

This project is using [Pyenv](https://github.com/pyenv/pyenv) for Python version management, but you probably don't need to set up Pyenv. The project was developed using the Python version in [.python-version](.python-version).

You can use [Obsidian](https://obsidian.md/) to edit the sample vault or create your own.

# Installation & Setup
1. Install project dependencies with
```
pipenv install
```

2. Change Obsidian's link format to absolute path:
- Click on "Open folder as vault" and navigate to the sample vault folder
- Open "Settings" (currently the gear in the bottom-left corner)
- Click on "Files & Links"
- Change "New link format" to "Absolute path in vault"

Note: you may need to change the link format to absolute on a vault-by-vault basis

# Building the Vault
Begin every leaf file with `#backlink:[[path|name]]`. If you're using Obsidian with "New link format" set to "Absolute path in vault" mode, Obsidian should automatically generate a correct link when you type `[[` and then select your desired file.

# Tracing a leaf back to the root node
Run beleaf as follows:
```
python ./beleaf.py PATH_TO_VAULT PATH_TO_LEAF
```
For example, to run an example in the sample vault, run:
```
python beleaf.py ./sample_obsidian_vault/ ./sample_obsidian_vault/Fish\ Attack/Joe\ reacts/Friend\ notices/Friend\ runs\ too.md
```
I've coded in handling relative paths that begin with "./" on Linux, but not on Windows. It should handle full paths as well as `$PWD/` on Linux.

Tweet at me if you have any questions or concerns! I'm on Twitter at [@tonyaajjackson](https://twitter.com/tonyaajjackson)